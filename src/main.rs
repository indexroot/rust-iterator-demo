fn sum(num_array:&[u32]) -> Option<u32>{
    let mut result:u32=0;
    for n in num_array {
        if result.overflowing_add(*n).1==true {
            return None;
        }
        result+=n;
    }
    Some(result)
}
fn main() {
    let n=[1,2,3,4,5];
    println!("{:#?}",sum(&n));

    let n=[1,u32::MAX,3,4,5];
    sum(&n);
    println!("{:#?}",sum(&n));
}
